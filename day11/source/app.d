import std.stdio;
import std.typecons : tuple;
import std.datetime;
import std.conv : to, text;
import std.array : array, split;
import std.algorithm;
import unit_threaded;
import core.thread : Thread;

void main()
{
	auto input = get_input("input.txt");
	writefln("part1: %s", input.part1);
	writefln("part1: %s", input.part2);
}

string[][] get_input(in string file)
{
	string[] lines = File(file).byLine.map!(a => a.text).array;
	string[][] result = lines.map!(a => a.split("")).array;
	return result;
}

unittest
{
	auto input = get_input("test_input.txt");
	input.part1.should == 37;
}

string[][] deepCopy(in string[][] toCopy)
{
	return toCopy.map!(a => a.dup).array;
}

int part1(string[][] input)
{
	string[][] copy = input.deepCopy;
	bool somethingChanged = true;
	while (somethingChanged)
	{
		string[][] change = copy.deepCopy;
		somethingChanged = false;
		// writeln(copy);
		foreach (y, row; copy)
		{
			foreach (x, value; row)
			{
				if (value == ".")
					continue;
				int occupied;
				foreach (check_x; -1 .. 2)
					foreach (check_y; -1 .. 2)
					{
						const proof_x = x + check_x;
						const proof_y = y + check_y;
						if (proof_x >= row.length || proof_x < 0)
							continue;
						if (proof_y >= copy.length || proof_y < 0)
							continue;
						if (check_x == 0 && check_y == 0)
							continue;
						if (copy[proof_y][proof_x] == "#")
							occupied++;
					}
				// writefln("occupied: %s", occupied);
				if (occupied == 0 && value == "L")
				{
					change[y][x] = "#";
					somethingChanged = true;
					continue;
				}
				if (occupied >= 4 && value == "#")
				{
					change[y][x] = "L";
					somethingChanged = true;
				}
			}
		}
		copy = change;
		// Thread.sleep(dur!"seconds"(5));
	}

	auto result = 0;
	foreach (ref row; copy)
		foreach (value; row)
		{
			if (value == "#")
				result++;
		}

	return result;
}

bool isThereAnOccupied(in string[][] input, int x, int y, int dirX, int dirY)
{
	for (int check_y = y + dirY, check_x = x + dirX; check_y >= 0 && check_y < input.length && check_x >= 0 && check_x < input[0]
			.length; check_y += dirY, check_x += dirX)
	{
		if (input[check_y][check_x] == "#")
			return true;
		if (input[check_y][check_x] == "L")
			return false;
	}
	return false;
}

int countOccupied(in string[][] input, int x, int y)
{
	auto checks = [
		tuple(1, 0), tuple(-1, 0), tuple(0, 1), tuple(0, -1), tuple(1, 1), tuple(-1, 1), tuple(1, -1), tuple(-1, -1),
	];
	int occupied;
	foreach (check; checks)
		if (isThereAnOccupied(input, x, y, check[0], check[1]))
			occupied++;
	return occupied;
}

unittest
{
	auto input = get_input("test_input_2.txt");
	input.countOccupied(3, 4).should == 8;
}

unittest
{
	writefln("test3");
	auto input = get_input("test_input_3.txt");
	input.countOccupied(1, 1).should == 0;
}

unittest
{
	auto input = get_input("test_input_4.txt");
	input.countOccupied(3, 3).should == 0;
}

int part2(string[][] input)
{
	string[][] copy = input.deepCopy;
	bool somethingChanged = true;
	while (somethingChanged)
	{
		string[][] change = copy.deepCopy;
		somethingChanged = false;
		// writeln(copy);
		foreach (y, row; copy)
		{
			foreach (x, value; row)
			{
				if (value == ".")
					continue;
				const int occupied = countOccupied(copy, cast(int) x, cast(int) y);
				if (occupied == 0 && value == "L")
				{
					change[y][x] = "#";
					somethingChanged = true;
					continue;
				}
				if (occupied >= 5 && value == "#")
				{
					change[y][x] = "L";
					somethingChanged = true;
				}
			}
		}
		copy = change;
		// writeln(copy);
		// Thread.sleep(dur!"seconds"(5));
	}

	auto result = 0;
	foreach (ref row; copy)
		foreach (value; row)
		{
			if (value == "#")
				result++;
		}

	return result;
}

unittest
{
	auto input = get_input("test_input.txt");
	input.part2.should == 26;
}
