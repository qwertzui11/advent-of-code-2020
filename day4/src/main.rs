fn main() {
    println!("part1: {}, part2: {}", part1(), part2());
}

fn part1() -> i32 {
    let lines = input();
    lines.iter().filter(|line| is_passport_valid(line)).count() as i32
}

fn input() -> Vec<String> {
    use std::fs::File;
    use std::io::{prelude::*, BufReader};
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut lines: Vec<String> = Vec::new();
    let mut passport = String::new();
    for line in reader.lines() {
        let line_string: String = line.unwrap();
        if line_string.is_empty() {
            lines.push(passport);
            passport = String::new();
            continue;
        }
        passport.push_str(" ");
        passport.push_str(&line_string);
    }
    lines
}

fn is_passport_valid(passport: &String) -> bool {
    let fields = ["ecl:", "pid:", "eyr:", "hcl:", "byr:", "iyr:", "hgt:"];
    let words: Vec<_> = passport.split_whitespace().collect();
    fields
        .iter()
        .all(|check| is_field_available(&words, *check))
}

fn is_field_available(fields: &Vec<&str>, field: &str) -> bool {
    fields.iter().any(|word| word.starts_with(field))
}

fn part2() -> i32 {
    let lines = input();
    lines
        .iter()
        .filter(|line| is_passport_valid_part2(line))
        .count() as i32
}

fn is_passport_valid_part2(passport: &String) -> bool {
    let fields = ["ecl:", "pid:", "eyr:", "hcl:", "byr:", "iyr:", "hgt:"];
    let words: Vec<_> = passport.split_whitespace().collect();
    fields
        .iter()
        .all(|check| is_field_available_and_valid(&words, *check))
}

fn is_field_available_and_valid(fields: &Vec<&str>, field: &str) -> bool {
    use regex::Regex;
    let found = fields.iter().find(|word| word.starts_with(field));
    if found == None {
        return false;
    }
    let check = found.unwrap();
    let value = check.strip_prefix(field).unwrap();
    let value_as_number = value.parse::<i32>().unwrap_or(0);
    let result = match field {
        "byr:" => value_as_number >= 1920 && value_as_number <= 2002,
        "iyr:" => value_as_number >= 2010 && value_as_number <= 2020,
        "eyr:" => value_as_number >= 2020 && value_as_number <= 2030,
        "hgt:" => {
            if value.ends_with("cm") {
                let stripped = value.strip_suffix("cm").unwrap();
                let height = stripped.parse::<i32>().unwrap();
                return height >= 150 && height <= 193;
            }
            if value.ends_with("in") {
                let stripped = value.strip_suffix("in").unwrap();
                let height = stripped.parse::<i32>().unwrap();
                return height >= 59 && height <= 76;
            }
            false
        }
        "hcl:" => {
            let reg = Regex::new("^#([a-f0-9]{6})$").unwrap();
            reg.is_match(&value)
        }
        "ecl:" => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&value),
        "pid:" => {
            let reg = Regex::new(r"^\d{9}$").unwrap();
            reg.is_match(&value)
        }
        _ => {
            assert!(false);
            false
        }
    };
    result
}
