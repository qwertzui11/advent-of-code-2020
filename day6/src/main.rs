fn main() {
    println!("part1: {}", part1());
    println!("part2: {}", part2());
}

fn input_part1() -> Vec<String> {
    use std::fs::File;
    use std::io::{prelude::*, BufReader};
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut lines: Vec<String> = Vec::new();
    let mut customs = String::new();
    for line in reader.lines() {
        let line_string: String = line.unwrap();
        if line_string.is_empty() {
            lines.push(customs);
            customs = String::new();
            continue;
        }
        customs.push_str(&line_string);
    }
    lines
}

fn part1() -> i32 {
    let customs = input_part1();
    customs
        .iter()
        .fold(0, |result, custom| result + count_answers_part1(custom)) as i32
}

fn count_answers_part1(custom: &String) -> i32 {
    let mut chars: Vec<_> = custom.chars().collect();
    chars.sort_by(|a, b| b.cmp(a));
    chars.dedup();
    chars.len() as i32
}

fn input_part2() -> Vec<Vec<String>> {
    use std::fs::File;
    use std::io::{prelude::*, BufReader};
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut lines: Vec<Vec<String>> = Vec::new();
    let mut customs: Vec<_> = Vec::new();
    for line in reader.lines() {
        let line_string: String = line.unwrap();
        if line_string.is_empty() {
            lines.push(customs);
            customs = Vec::new();
            continue;
        }
        customs.push(line_string);
    }
    lines
}

fn part2() -> i32 {
    let customs = input_part2();
    customs
        .iter()
        .fold(0, |result, custom| result + count_answers_part2(custom)) as i32
}

fn count_answers_part2(custom: &Vec<String>) -> i32 {
    use std::collections::HashMap;

    let mut people: Vec<Vec<char>> = custom.iter().map(|cast| cast.chars().collect()).collect();
    let mut letters = HashMap::new();
    for person in people.iter_mut() {
        person.sort_by(|a, b| b.cmp(a));
        person.dedup();
    }
    for person in people {
        for letter in person {
            let entry = letters.entry(letter).or_insert(0);
            *entry += 1;
        }
    }
    letters
        .iter()
        .filter(|check| *check.1 == custom.len())
        .count() as i32
}
