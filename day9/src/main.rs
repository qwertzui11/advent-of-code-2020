fn main() {
    let input = get_input("input.txt");
    println!("part1: {}", part1(&input, 25).1);
    println!("part2: {}", part2(&input, 25));
}

fn get_input(file: &str) -> Vec<i64> {
    use std::io::{prelude::*, BufReader};
    let file = std::fs::File::open(file).unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .map(|cast| cast.unwrap().parse::<i64>().unwrap())
        .collect()
}

#[test]
fn test_part1() {
    let input = get_input("test_input.txt");
    assert_eq!(part1(&input, 5).1, 127);
}

fn part1(numbers: &Vec<i64>, premable: usize) -> (usize, i64) {
    for number in numbers.iter().enumerate() {
        let index = number.0;
        if index < premable {
            continue;
        }
        let last_five: Vec<i64> = numbers[index - premable..index].to_vec();
        if !is_number_sum_of_two(*number.1, &last_five) {
            return (number.0, *number.1);
        }
    }
    assert!(false);
    (0, 0)
}

fn is_number_sum_of_two(check: i64, numbers: &Vec<i64>) -> bool {
    // println!("check: {:?}, numbers: {:?}", check, &numbers);
    for first in numbers {
        for second in numbers {
            if first == second {
                continue;
            }
            if first + second == check {
                return true;
            }
        }
    }
    false
}

fn part2(numbers: &Vec<i64>, premable: usize) -> i64 {
    let invalid_number = part1(numbers, premable);
    for first in numbers[0..invalid_number.0 - 2].iter().enumerate() {
        let mut calc: Vec<i64> = Vec::new();
        for second in numbers[first.0..].iter() {
            let sum: i64 = calc.iter().sum();
            if sum == invalid_number.1 {
                return calc.iter().min().unwrap() + calc.iter().max().unwrap();
            }
            if sum > invalid_number.1 {
                break;
            }
            calc.push(*second);
        }
    }
    assert!(false);
    0
}

#[test]
fn test_part2() {
    let input = get_input("test_input.txt");
    assert_eq!(part2(&input, 5), 62);
}
