fn main() {
    println!("part1: {}", part1("input.txt"));
    println!("part2: {}", part2("input.txt"));
}

fn get_input_from_file(file: &str) -> Vec<(String, i32)> {
    use std::io::{prelude::*, BufReader};
    let file = std::fs::File::open(file).unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .map(|cast| cast.unwrap())
        .map(|cast| line_to_operation(&cast))
        .collect()
}

fn part1(file: &str) -> i32 {
    let input = get_input_from_file(file);
    execute_till_end_or_repeat(&input).0
}

fn execute_till_end_or_repeat(input: &Vec<(String, i32)>) -> (i32, bool) {
    use std::collections::HashSet;
    let mut already_executed = HashSet::new();
    let mut accumulator = 0;
    let mut index: i32 = 0;
    while !already_executed.contains(&index) {
        already_executed.insert(index);
        if index as usize >= input.len() {
            break;
        }
        let operation = &input[index as usize];
        if operation.0 == "nop" {
            index += 1;
        }
        if operation.0 == "acc" {
            accumulator += operation.1;
            index += 1;
        }
        if operation.0 == "jmp" {
            index += operation.1;
        }
    }
    let ended = index == (input.len() as i32);
    (accumulator, ended)
}

fn line_to_operation(line: &str) -> (String, i32) {
    let words: Vec<_> = line.split(" ").collect();
    (words[0].to_string(), words[1].parse::<i32>().unwrap())
}

#[test]
fn test_part1() {
    assert_eq!(part1("test_input.txt"), 5);
}

fn part2(file: &str) -> i32 {
    let input = get_input_from_file(file);
    let mut last_changed: usize = 0;
    loop {
        if input[last_changed].0 == "acc" {
            last_changed += 1;
            continue;
        }
        let mut input_copy = input.clone();
        let item = &mut input_copy[last_changed];
        if item.0 == "jmp" {
            item.0 = "nop".to_string();
        } else {
            item.0 = "jmp".to_string();
        }
        let result = execute_till_end_or_repeat(&input_copy);
        if result.1 {
            break result.0;
        }
        last_changed += 1;
    }
}

#[test]
fn test_part2() {
    assert_eq!(part2("test_input.txt"), 8);
}
