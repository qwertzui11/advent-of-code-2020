import std.stdio;
import std.algorithm;
import std.range;
import std.conv : to;
import std.array : array;

void main()
{
  writeln("first: ", input.calculate_first);
  writeln("second: ", input.calculate_second);
}

int[] input()
{
  return File("input.txt").byLine.map!(to!int).array;
}

int calculate_first(int[] check)
{
  foreach (first; check)
    foreach (second; check)
      if (first + second == 2020)
        return first * second;
  assert(false);
}

unittest
{
  import unit_threaded;

  int[] testInput = [1721, 979, 366, 299, 675, 1456];
  testInput.calculate.should == 514_579;
}

int calculate_second(int[] check)
{
  foreach (first; check)
    foreach (second; check)
      foreach (third; check)
        if (first + second + third == 2020)
          return first * second * third;
  assert(false);
}
