use std::fs::File;

fn main() {
    let input = read_input("input.txt");
    println!("part1: {}, part2: {}", part1(&input), part2(&input));
}

fn part2(input: &Vec<Vec<Field>>) -> i64 {
    let steps = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    steps.iter().fold(1, |result, step| {
        result * count_trees(&input, step.0, step.1)
    })
}

#[test]
fn test_part2() {
    let input = read_input("test_input.txt");
    assert_eq!(part2(&input), 336);
}

fn count_trees(input: &Vec<Vec<Field>>, step_x: usize, step_y: usize) -> i64 {
    let way = calculate_way(&input, step_x, step_y);
    tree_count_on_way(&way)
}

fn tree_count_on_way(input: &Vec<Field>) -> i64 {
    input.iter().fold(0, |result, field| {
        if *field == Field::Tree {
            return result + 1;
        }
        result
    })
}

fn part1(input: &Vec<Vec<Field>>) -> i64 {
    count_trees(&input, 3, 1)
}

#[test]
fn test_part1_test_input() {
    let input = read_input("test_input.txt");
    assert_eq!(part1(&input), 7);
}

#[test]
fn test_part1_real_input() {
    let input = read_input("input.txt");
    assert_eq!(part1(&input), 173);
}

fn calculate_way(input: &Vec<Vec<Field>>, step_x: usize, step_y: usize) -> Vec<Field> {
    // I image with `scan` I can refactor the `for` away
    // https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.scan
    //
    // Or better, `enumerate` and calculate the `x = step_x * index`
    let mut x = 0;
    let mut result: Vec<Field> = Vec::new();
    for line in input.iter().step_by(step_y) {
        result.push(*line.iter().cycle().nth(x).unwrap());
        x += step_x;
    }
    result
}

fn read_input(file_name: &str) -> Vec<Vec<Field>> {
    use std::io::{prelude::*, BufReader};
    let file = File::open(file_name).unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .map(|line| parse_line(line.unwrap()))
        .collect()
}

#[test]
fn test_read_input() {
    let check = read_input("test_input.txt");
    assert_eq!(check[0][0], Field::Sqaure);
    assert_eq!(check[0][2], Field::Tree);
    assert_eq!(check[1][1], Field::Sqaure);
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum Field {
    Tree,
    Sqaure,
}

fn parse_line(line: String) -> Vec<Field> {
    line.chars().map(|parse| parse_character(parse)).collect()
}

fn parse_character(check: char) -> Field {
    match check {
        '#' => Field::Tree,
        _ => Field::Sqaure,
    }
}
