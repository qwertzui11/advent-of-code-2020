fn main() {
    println!("parsing input");
    let input = parse_file("input.txt");
    println!("parsed input");
    println!("part1: {}", count_gold_bags(&input));
    println!("part2: {}", count_bags_in_gold(&input));
}

#[test]
fn test_count_bags_in_gold() {
    let input = parse_file("test_input.txt");
    assert_eq!(count_bags_in_gold(&input), 32);
}

fn count_bags_in_gold(bags: &Vec<Bag>) -> i32 {
    // -1 because we must not count the shiny gold bag
    count_bags_recursive(bags, "shiny gold") - 1
}

fn count_bags_recursive(bags: &Vec<Bag>, current: &str) -> i32 {
    let current_bag = bags.iter().find(|find| find.color == current).unwrap();
    current_bag.contains.iter().fold(0, |result, check| {
        result + check.count * count_bags_recursive(bags, &check.color)
    }) + 1
}

#[test]
fn test_count_gold_bags() {
    let input = parse_file("test_input.txt");
    assert_eq!(count_gold_bags(&input), 4);
}

fn count_gold_bags(bags: &Vec<Bag>) -> i32 {
    let mut contains_gold: Vec<String> = bags
        .iter()
        .filter(|check| {
            check
                .contains
                .iter()
                .find(|find| find.color == "shiny gold")
                .is_some()
        })
        .map(|check| check.color.clone())
        .collect();
    loop {
        let old_len = contains_gold.len();

        for bag in bags {
            if contains_gold.contains(&bag.color) {
                continue;
            }
            for check in contains_gold.iter() {
                if bag
                    .contains
                    .iter()
                    .find(|find| &find.color == check)
                    .is_none()
                {
                    continue;
                }
                contains_gold.push(bag.color.clone());
                break;
            }
        }

        if old_len == contains_gold.len() {
            break old_len as i32;
        }
    }
}

fn parse_file(file: &str) -> Vec<Bag> {
    use std::fs::File;
    use std::io::{prelude::*, BufReader};
    let file = File::open(file).unwrap();
    let reader = BufReader::new(file);
    let line_parser = LineParser::new();
    reader
        .lines()
        .map(|line| line_parser.parse(&line.unwrap()))
        .collect()
}

use regex::Regex;
struct LineParser {
    line_regex: Regex,
    contains_regex: Regex,
}

impl LineParser {
    fn new() -> LineParser {
        LineParser {
            line_regex: Regex::new(r"^(?P<color>\w+ \w+) bags contain (?P<contains>.*)$").unwrap(),
            contains_regex: Regex::new(r"((?P<count>\d+) (?P<color>\w+ \w+) bags?[,.])").unwrap(),
        }
    }

    fn parse(&self, line: &str) -> Bag {
        let mut result = Bag {
            color: String::new(),
            contains: Vec::new(),
        };
        assert!(self.line_regex.is_match(line));
        for capture_line in self.line_regex.captures_iter(line) {
            result.color = capture_line["color"].to_string();
            let contains = &capture_line["contains"];
            if contains == "no other bags." {
                break;
            }
            assert!(self.contains_regex.is_match(line));
            for capture_contains in self.contains_regex.captures_iter(contains) {
                result.contains.push(NumberOfBags {
                    color: capture_contains["color"].to_string(),
                    count: capture_contains["count"].parse::<i32>().unwrap(),
                });
            }
        }

        result
    }
}

struct Bag {
    color: String,
    contains: Vec<NumberOfBags>,
}

struct NumberOfBags {
    color: String,
    count: i32,
}

#[test]
fn test_parse_line() {
    println!("Hello, world!");
    let line_parser = LineParser::new();
    let check =
        line_parser.parse("light red bags contain 1 bright white bag, 2 muted yellow bags.");
    assert_eq!(check.color, "light red");
    assert_eq!(check.contains.len(), 2);
    assert_eq!(check.contains[0].color, "bright white");
    assert_eq!(check.contains[0].count, 1);
    assert_eq!(check.contains[1].color, "muted yellow");
    assert_eq!(check.contains[1].count, 2);
}
