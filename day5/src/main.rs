fn main() {
    println!("part1: {}, part2: {}", part1(), part2());
}

fn part2() -> i32 {
    use std::collections::HashSet;

    let mut seats = HashSet::new();
    for seat in get_input().map(|line| seat_id(&line.unwrap())) {
        seats.insert(seat);
    }
    for check in 0..(8 * 128 + 8) {
        let previous = check - 1;
        let next = check + 1;
        if !seats.contains(&check) && seats.contains(&previous) && seats.contains(&next) {
            return check;
        }
    }
    assert!(false);
    42
}

fn part1() -> i32 {
    get_input()
        .map(|line| seat_id(&line.unwrap()))
        .max()
        .unwrap()
}

fn get_input() -> std::io::Lines<std::io::BufReader<std::fs::File>> {
    use std::io::{prelude::*, BufReader};
    let file = std::fs::File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    reader.lines()
}

fn seat_id(description: &str) -> i32 {
    column(description) + row(description) * 8
}

#[test]
fn test_seat_id() {
    assert_eq!(seat_id("FBFBBFFRLR"), 357);
    assert_eq!(seat_id("FFFBBBFRRR"), 119);
    assert_eq!(seat_id("BBFFBBFRLL"), 820);
}

fn column(description: &str) -> i32 {
    let column_description: Vec<char> = description.chars().skip(7).collect();
    let mut lower_bound = 0;
    let mut upper_bound = 8;
    for column in column_description.iter() {
        let diff_upper_lower = (upper_bound - lower_bound) / 2;
        if *column == 'L' {
            upper_bound -= diff_upper_lower;
        }
        if *column == 'R' {
            lower_bound += diff_upper_lower;
        }
        //println!(
        //    "row: {}, lower_bound: {}, upper_bound: {}",
        //    column, lower_bound, upper_bound
        //);
    }
    return lower_bound;
}

#[test]
fn test_column_0() {
    assert_eq!(column("FBFBBFFRLR"), 5);
}

fn row(description: &str) -> i32 {
    let row_description: Vec<char> = description.chars().take(7).collect();
    let mut lower_bound = 0;
    let mut upper_bound = 128;
    for row in row_description.iter() {
        let diff_upper_lower = (upper_bound - lower_bound) / 2;
        if *row == 'F' {
            upper_bound -= diff_upper_lower;
        }
        if *row == 'B' {
            lower_bound += diff_upper_lower;
        }
        //println!(
        //    "row: {}, lower_bound: {}, upper_bound: {}",
        //    row, lower_bound, upper_bound
        //);
    }
    return lower_bound;
}

#[test]
fn test_row_0() {
    assert_eq!(row("FBFBBFFRLR"), 44);
}
