fn main() {
    let input = get_input("input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2_fast(&input));
}

fn get_input(file: &str) -> Vec<i64> {
    use std::io::{prelude::*, BufReader};
    let file = std::fs::File::open(file).unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .map(|cast| cast.unwrap().parse::<i64>().unwrap())
        .collect()
}

#[test]
fn test_part1() {
    let input = get_input("test_input.txt");
    assert_eq!(part1(&input), 220);
}

fn part1(numbers: &Vec<i64>) -> i64 {
    let mut numbers_copy = numbers.clone();
    numbers_copy.push(0);
    numbers_copy.sort();
    numbers_copy.push(numbers_copy[numbers_copy.len() - 1] + 3);
    let ones_and_thress: Vec<_> = numbers_copy
        .iter()
        .zip(&numbers_copy[1..])
        .map(|calc| calc.1 - calc.0)
        .filter(|check| *check == 1 || *check == 3)
        .collect();
    let ones = ones_and_thress.iter().filter(|check| **check == 1).count() as i64;
    let threes = ones_and_thress.iter().filter(|check| **check == 3).count() as i64;
    ones * threes
}

fn part2(numbers: &Vec<i64>) -> i64 {
    let mut numbers_copy = numbers.clone();
    numbers_copy.sort();
    let target = numbers_copy[numbers_copy.len() - 1] + 3;
    part2_recursive(&numbers_copy, 0, target)
}

#[test]
fn test_part2_2() {
    let input = get_input("test_input_2.txt");
    assert_eq!(part2(&input), 8);
}

#[test]
fn test_part2() {
    let input = get_input("test_input.txt");
    assert_eq!(part2(&input), 19208);
}

fn part2_fast(numbers: &[i64]) -> i64 {
    use std::collections::HashMap;

    let mut input = numbers.to_vec();
    input.push(0);
    input.sort();
    input.push(input[input.len() - 1] + 3);
    println!("{:?}", input);

    let mut cache: HashMap<usize, i64> = HashMap::new();
    cache.insert(0, 1);
    let mut result: i64 = 0;
    for item in input.iter().enumerate() {
        println!("item: {}, {}", item.0, item.1);
        if item.0 < input.len() {
            if item.0 > 0 {
                let check = input[item.0 - 1];
                let diff = item.1 - check;
                if diff == 1 || diff == 2 || diff == 3 {
                    let index: usize = item.0 - 1;
                    let prev = cache[&index];
                    let add = cache.entry(item.0).or_insert(0);
                    *add += prev;
                    result = *add;
                }
            }
            if item.0 > 1 {
                let check = input[item.0 - 2];
                let diff = item.1 - check;
                if diff == 2 || diff == 3 {
                    let index: usize = item.0 - 2;
                    let prev = cache[&index];
                    let add = cache.entry(item.0).or_insert(0);
                    *add += prev;
                    result = *add;
                }
            }
            if item.0 > 2 {
                let check = input[item.0 - 3];
                let diff = item.1 - check;
                if diff == 3 {
                    let index: usize = item.0 - 3;
                    let prev = cache[&index];
                    let add = cache.entry(item.0).or_insert(0);
                    *add += prev;
                    result = *add;
                }
            }
        }
    }
    println!("{:?}", cache);
    //println!("{:?}", cache[input[input.len() - 1]]);
    result as i64
}

// #[test]
// fn test_part2_fast() {
//     let input = get_input("test_input.txt");
//     assert_eq!(part2_fast(&input), 19208);
// }

#[test]
fn test_part2_fast_2() {
    let input = get_input("test_input_2.txt");
    assert_eq!(part2_fast(&input), 8);
}

fn part2_recursive(numbers: &[i64], last: i64, target: i64) -> i64 {
    let mut result: i64 = 0;
    if numbers.is_empty() {
        return 1;
    }
    for counter in 0..3 {
        //println!("{:?}, {}, {}", numbers, last, target);
        if counter >= numbers.len() {
            break;
        }
        let current = numbers[counter];
        let diff = numbers[counter] - last;
        //println!("{}, {}", current, diff);
        if diff <= 3 {
            result += part2_recursive(&numbers[(counter + 1)..], current, target);
        }
    }
    result
}
