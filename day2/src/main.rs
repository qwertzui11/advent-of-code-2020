use std::fs::File;

fn main() {
    use std::io::{prelude::*, BufReader};

    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);

    let mut passwords: Vec<PasswordEntry> = Vec::new();
    passwords.extend(reader.lines().map(|line| parse_line(line.unwrap())));
    println!("part one: {}", valid_count(&passwords));
    println!("part two: {}", valid_positions(&passwords));
}

#[derive(Debug)]
struct PasswordEntry {
    minimum: i32,
    maximum: i32,
    letter: char,
    word: String,
}

fn parse_line(line: String) -> PasswordEntry {
    let mut words = Vec::new();
    words.extend(line.split(|check| is_whitespace_or_dash(check)));
    PasswordEntry {
        minimum: words[0].parse::<i32>().unwrap(),
        maximum: words[1].parse::<i32>().unwrap(),
        letter: words[2].chars().next().unwrap(),
        word: words[3].to_string(),
    }
}

fn is_whitespace_or_dash(check: char) -> bool {
    check == ' ' || check == '-'
}

#[test]
fn test_parse_line() {
    let check = parse_line("1-3 a: abcde".to_string());
    assert_eq!(check.word, "abcde");
    assert_eq!(check.letter, 'a');
    assert_eq!(check.minimum, 1);
    assert_eq!(check.maximum, 3);
}

fn valid_count(entries: &[PasswordEntry]) -> i32 {
    entries
        .iter()
        .map(|entry| is_valid(entry))
        .filter(|valid| *valid)
        .count() as i32
}

fn is_valid(entry: &PasswordEntry) -> bool {
    let letter_count = entry
        .word
        .chars()
        .filter(|validate| *validate == entry.letter)
        .count() as i32;
    // min(max(letter_count, entry.minimum), entry.maximum) == letter_count;
    letter_count >= entry.minimum && letter_count <= entry.maximum
}

#[test]
fn test_valid_count() {
    let input = [PasswordEntry {
        minimum: 1,
        maximum: 3,
        letter: 'a',
        word: "abcde".to_string(),
    }];
    assert_eq!(valid_count(&input), 1);
}

fn valid_positions(entries: &[PasswordEntry]) -> i32 {
    entries
        .iter()
        .map(|entry| is_valid_position(entry))
        .filter(|valid| *valid)
        .count() as i32
}

fn is_valid_position(entry: &PasswordEntry) -> bool {
    let first_index: usize = (entry.minimum - 1) as usize;
    let second_index: usize = (entry.maximum - 1) as usize;
    // println!( "entry: {:?}, first_index: {}, second_index: {}", entry, first_index, second_index);
    (entry.word.chars().nth(first_index).unwrap() == entry.letter)
        != (entry.word.chars().nth(second_index).unwrap() == entry.letter)
}

#[test]
fn test_is_valid_position_fail() {
    let input = PasswordEntry {
        minimum: 1,
        maximum: 3,
        letter: 'a',
        word: "abcde".to_string(),
    };
    assert_eq!(is_valid_position(&input), true);
}

#[test]
fn test_is_valid_position_success() {
    let input = PasswordEntry {
        minimum: 1,
        maximum: 3,
        letter: 'b',
        word: "cdefg".to_string(),
    };
    assert_eq!(is_valid_position(&input), false);
}

#[test]
fn test_is_valid_position_fail_2() {
    let input = PasswordEntry {
        minimum: 17,
        maximum: 20,
        letter: 'x',
        word: "zsxjrxkgxxxxxxxmxgxf".to_string(),
    };
    assert_eq!(is_valid_position(&input), true);
}
