import std.stdio;
import std.typecons : tuple;
import std.datetime;
import std.conv : to, text;
import std.array : array, split;
import std.algorithm;
import std.range;
import unit_threaded;
import core.thread : Thread;

import std.stdio;

void main()
{
	auto input = get_input("input.txt");
	writefln("part1: %s", part1(input));
}

string[] get_input(in string file)
{
	string[] lines = File(file).byLine.map!(a => a.text).array;
	return lines;
}

long part1(in string[] input)
{
	long[long] numbers;
		string mask;
	foreach (line; input)
	{
		immutable maskPrefix = "mask = ";
		if (line.startsWith(maskPrefix))
		{
			mask = line[maskPrefix.length .. $];
			// writefln("mask: %s", mask);
		}
		if (line.startsWith("mem"))
		{
			import std.regex;
			import std.format;
			import std.string;

			assert(!mask.empty);
			const match = line.matchFirst(r"mem\[(\d+)\] = (\d+)");
			assert(match);
			// writefln("match[1]: %s, match[2]: %s", match[1], match[2]);
			long index = match[1].to!long;
			const string number = format("%b", match[2].to!long);
			// writefln("number: %s", mask);
			// writefln("number: %s", mask.split(""));
			string fixedNumber = (generate!(() => '0')().take(36 - number.length).array ~ number ).retro.zip(mask.retro).map!(a => {
					// writefln("a: %s", a);
					if (a[1] == 'X') return a[0];
					return a[1];
					}()).text;
			// writefln("fixedNumber: %s", fixedNumber);
			const fixedNumberCasted = fixedNumber.retro.to!long(2);
			numbers[index] = fixedNumberCasted;
		}
	}
	return numbers.values.sum;
}

unittest {
	const input = get_input("test_input.txt");
	input.part1.should == 165;
}

